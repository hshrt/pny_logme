<?php
include("config.php");
include("opendb.php");

/////////////////////////////////////////////////////////

function twoDigitMe($n) {
	if ($n < 10) {
		return "0".$n;
	} else {
		return $n;
	}
}


function h0sta($room,$h0,$time) {
	
	$cfgDBHost = "localhost";
	$cfgDBPort = "3306";
	$cfgDBUser = "kahoo";
	$cfgDBPw   = "000000";
	$cfgDBUse  = "LogME";
	
	mysql_connect($cfgDBHost.":".$cfgDBPort,$cfgDBUser,$cfgDBPw);
	@mysql_select_db($cfgDBUse) or die( "Unable to select database");	
	
	$sta = trim($h0);
	$data = trim($h0);
	
	$cpuroom = "".twoDigitMe(hexdec(substr($data,0,2))).twoDigitMe(hexdec(substr($data,2,2)));	
	$fax = "".substr($data,92,12);		

	$service = ((( hexdec(substr($data,4,2)) & 1) == 1) + 0);
	$masterbit = ((( hexdec(substr($data,4,2)) & 2) == 2) + 0);
	$valet = ((( hexdec(substr($data,4,2)) & 4) == 4) + 0);
	
	$faxbit = ((( hexdec(substr($data,4,2)) & 8) == 8) + 0);
	$msgbit = (((hexdec(substr($data,68,2)) & 1) == 1) + 0);

	$dnd = ((( hexdec(substr($data,4,2)) & 16) == 16) + 0);
	$mur = ((( hexdec(substr($data,4,2)) & 32) == 32) + 0);
	
	$delivery = ((( hexdec(substr($data,4,2)) & 64) == 64) + 0);
	$tdnd = ((( hexdec(substr($data,4,2)) & 128) == 128) + 0);

	$cf = (((hexdec(substr($data,68,2))) & 128) == 0 ? "C" : "F");
	$cpureset = (((hexdec(substr($data,68,2))) & 64) == 0);

	$spa = ((( hexdec(substr($data,72,2)) & 128) == 128) + 0);

    $langtmp = hexdec(substr($data,30,2));
    if($langtmp == 0)
            $lang = "English";
    else if($langtmp == 1)
            $lang = "Japanese";
    else if($langtmp == 2)
            $lang = "Traditional Chinese";
    else if($langtmp == 3)
            $lang = "Simple Chinese";
    else if($langtmp == 4)
            $lang = "German";
    else if($langtmp == 5)
            $lang = "French";
    else if($langtmp == 6)
            $lang = "Spanish";
    else if($langtmp == 7)
            $lang = "Italian";
    else if($langtmp == 8)
            $lang = "Arabian";
    else if($langtmp == 9)
            $lang = "Korean";
    else if($langtmp == 0x0A )
            $lang = "Russian";
    else if($langtmp == 0x0B)
            $lang = "Portuguese";


	$foyerlightlevel = hexdec(substr($data,6,2));
	$dressinglightlevel = hexdec(substr($data,8,2));
	$bathroomlightlevel = hexdec(substr($data,10,2));
	$bedroomlightlevel = hexdec(substr($data,12,2));
	$bedheadlightlevel = hexdec(substr($data,14,2));
	$livinglightlevel = hexdec(substr($data,16,2));
	$vanitysconcelevel = hexdec(substr($data,54,2));
	
	$nightlight = ((( hexdec(substr($data,78,2)) & 64) == 64) + 0);
	$rightreadinglight = ((( hexdec(substr($data,78,2)) & 1) == 1) + 0);
	$leftreadinglight = ((( hexdec(substr($data,78,2)) & 2) == 2) + 0);
	$powerlight = (((hexdec(substr($data,68,2)) & 16) == 16) + 0);

    $p15feedbackdimming = hexdec(substr($data,84,2));
    $bedroomdimming = hexdec(substr($data,36,2));
    $masterdimming = hexdec(substr($data,38,2));
    $dressingdimming = hexdec(substr($data,40,2));
    $bathroomdimming = hexdec(substr($data,42,2));
    $livingdimming = hexdec(substr($data,44,2));

	$outdoortemperature = hexdec(substr($data,18,2));
    $outdoorhumidity = hexdec(substr($data,20,2));

	$windchill = hexdec(substr($data,34,2));
    $windspeed = twoDigitMe(hexdec(substr($data,22,2))).twoDigitMe(hexdec(substr($data,24,2)));

  
	$winddirtmp = ((hexdec(substr($data,26,2))) & 0x0F);
    if($winddirtmp == 1)
            $winddir = "N";
    else if($winddirtmp == 2)
            $winddir = "NE";
    else if($winddirtmp == 4)
            $winddir = "E";
    else if($winddirtmp == 8)
            $winddir = "SE";
    else if($winddirtmp == 16)
            $winddir = "S";
    else if($winddirtmp == 32)
            $winddir = "SW";
    else if($winddirtmp == 64)
            $winddir = "W";
    else if($winddirtmp == 128)
            $winddir = "NW";
    else
    		$winddir = $winddirtmp;
       

    $weatherstatmp = ((hexdec(substr($data,28,2))) & 0x0F);
    if($weatherstatmp == 1)
           	$weathersta = "Clear Day";
    else if($weatherstatmp == 2)
            $weathersta = "Cloudy";
    else if($weatherstatmp == 3)
            $weathersta = "Rain";
    else if($weatherstatmp == 4)
            $weathersta = "Sleet";
    else if($weatherstatmp == 5)
            $weathersta = "Partly Cloudy";
    else if($weatherstatmp == 6)
            $weathersta = "Partly Cloudy Night";
    else if($weatherstatmp == 7)
            $weathersta = "Heavy Rain";
    else if($weatherstatmp == 8)
            $weathersta = "Clear Night";
    else if($weatherstatmp == 9)
            $weathersta = "Snowy";
    else if($weatherstatmp == 0x0A)
            $weathersta = "Storm";
    else if($weatherstatmp == 0x0B)
            $weathersta = "Storm Alert";
    else
    		$weathersta = $weatherstatmp;


	$uv = hexdec(substr($data,32,2));
	

	$dressavtmp = ((hexdec(substr($data,70,2))) & 0x0F);
	if($dressavtmp == 0)
		$dressav = "OFF";
	else if($dressavtmp == 1)
		$dressav = "TV";
	else if($dressavtmp == 2)
		$dressav = "AV";
	else if($dressavtmp == 3)
		$dressav = "iRadio";
	else if($dressavtmp == 4)
		$dressav = "AUX";
		
	
	$dressmute = ((( hexdec(substr($data,70,2)) & 16) == 16) + 0);
	$dressvol = hexdec(substr($data,48,2));

	
	$bathavtmp = ((hexdec(substr($data,72,2))) & 0x0F);
	if($bathavtmp == 0)
		$bathav = "OFF";
	else if($bathavtmp == 1)
		$bathav = "TV";
	else if($bathavtmp == 2)
		$bathav = "AV";
	else if($bathavtmp == 3)
		$bathav = "iRadio";
	else if($bathavtmp == 4)
		$bathav = "AUX";

	$bathmute = ((( hexdec(substr($data,72,2)) & 16) == 16) + 0);		
	$bathvol =hexdec(substr($data,46,2));
	

	$maxtemp = hexdec(substr($data,80,2));
    $mintemp = hexdec(substr($data,82,2));

	$brfantmp = ((hexdec(substr($data,64,2))) & 0x0F);
	if($brfantmp == 0x00)
		$brfan = "OFF";
	else if($brfantmp == 0x01)
		$brfan = "LOW";
	else if($brfantmp == 0x02)
		$brfan = "MID";
	else if($brfantmp == 0x03)
		$brfan = "HI";
	else
		$brfan = hexdec(substr($data,64,2));

		
	$lrfantmp = ((hexdec(substr($data,66,2))) & 0x0F);
	if($lrfantmp == 0x00)
		$lrfan = "OFF";
	else if($lrfantmp == 0x01)
		$lrfan = "LOW";
	else if($lrfantmp == 0x02)
		$lrfan = "MID";
	else if($lrfantmp == 0x03)
		$lrfan = "HI";
	else
		$lrfan = hexdec(substr($data,64,2));


	$brtemp = hexdec(substr($data,74,2));
	$brtempmeasure = hexdec(substr($data,86,2));
	$brhotvalve =  hexdec(substr($data,104,2));	//( (hexdec(substr($data,104,2))) == 0x00 ? 0 : 1  );
	$brcoldvalve =  hexdec(substr($data,106,2));	//( (hexdec(substr($data,106,2))) == 0x00 ? 0 : 1 );


 $brhottemp = (hexdec(substr($data,104,2)));
        if($brhottemp == 99)
                $brhotvalvesta = "OFF";
        else 
                $brhotvalvesta = "ON";

 $brcoltemp = (hexdec(substr($data,106,2)));
        if($brcoldtemp == 0)
                $brcoldvalvesta = "OFF";
        else 
                $brcoldvalvesta = "ON";




	$lrtemp = hexdec(substr($data,76,2));
	$lrtempmeasure = hexdec(substr($data,88,2));
	$lrhotvalve =	 hexdec(substr($data,108,2));		// ( (hexdec(substr($data,108,2))) == 0x00 ? 0 : 1 );
    $lrcoldvalve = 	 hexdec(substr($data,110,2));		//( (hexdec(substr($data,110,2))) == 0x00 ? 0 : 1 );


    $cpudatetime = "20".twoDigitMe(hexdec(substr($data,56,2)))."-".twoDigitMe((hexdec(substr($data,58,2))) & 0x0F)."-".twoDigitMe(hexdec(substr($data,60,2)))." ".twoDigitMe(hexdec(substr($data,50,2))).":".twoDigitMe(hexdec(substr($data,52,2))).":00";

    
	$strQuery = "insert into cpustahistory values ('".$room."','".$time."','".$cpuroom."','".$fax."','".$service."',";
	$strQuery = $strQuery."'".$valet."','".$dnd."','".$mur."','".$delivery."','".$tdnd."',";
	$strQuery = $strQuery."'".$masterbit."','".$faxbit."','".$msgbit."','".$cf."','".$spa."','".$lang."',";
	$strQuery = $strQuery."'".$foyerlightlevel."','".$dressinglightlevel."','".$bathroomlightlevel."','".$bedroomlightlevel."',";
	$strQuery = $strQuery."'".$bedheadlightlevel."','".$livinglightlevel."','".$vanitysconcelevel."','".$nightlight."',";
	$strQuery = $strQuery."'".$rightreadinglight."','".$leftreadinglight."','".$powerlight."',";
	$strQuery = $strQuery."'".$p15feedbackdimming."','".$bedroomdimming."','".$masterdimming."','".$dressingdimming."',";
	$strQuery = $strQuery."'".$bathroomdimming."','".$livingdimming."','".$outdoortemperature."',";
	$strQuery = $strQuery."'".$outdoorhumidity."','".$windchill."','".$windspeed."','".$winddir."',";
	$strQuery = $strQuery."'".$weathersta."','".$dressav."','".$dressmute."','".$dressvol."',";
	$strQuery = $strQuery."'".$bathav."','".$bathmute."','".$bathvol."','".$maxtemp."','".$mintemp."',";
	$strQuery = $strQuery."'".$brfan."','".$lrfan."','".$brtemp."','".$brtempmeasure."','".$brhotvalve."',";
	$strQuery = $strQuery."'".$brcoldvalve."','".$lrtemp."','".$lrtempmeasure."','".$lrhotvalve."',";
	$strQuery = $strQuery."'".$lrcoldvalve."','".$cpudatetime."','".$cpureset."','".$sta."')";	
	
	
	/*
	
	$q = "insert into cpustahistory ( room, cpuroom, updatedOn, valet, dnd, mur, service, tdnd, ";
	$q = $q." brtemp, brfan, brtempmeasure, brcoldvalve, brhotvalve, ";
	$q = $q." lrtemp, lrfan, lrtempmeasure, lrcoldvalve, lrhotvalve, ";
	$q = $q." lang, cf, sta ";
	$q = $q." ) ";
	$q = $q." values ( '".$room."', '".$cpuroom."', '".$time."','".$valet."','".$dnd."','".$mur."','".$valetbox."','".$tdnd."', ";
	$q = $q." '".$brtemp."', '".$brfan."', '".$brtempmeasure."', '".$brcoldvalve."', '".$brhotvalve."', ";
	$q = $q." '".$lrtemp."', '".$lrfan."', '".$lrtempmeasure."', '".$lrcoldvalve."', '".$lrhotvalve."', ";
	$q = $q." '".$lang."', '".$cf."', '".$sta."' ";
	$q = $q." ) ; ";
	
	
	*/

	//echo "[".$q."]\n\n";
	
	$rs = mysql_query($strQuery);
	
	
	mysql_close();
	return true;
	
}


function chkSta() {

	$strQuery = "SELECT allroom.roomNum as room , COALESCE(sta_str, '-1') AS h0 , updatedOn";
        $strQuery = $strQuery ." from allroom  left join sta_file using( roomNum ) order by allroom.roomNum asc";
        
        $rs = mysql_query($strQuery);

        for($i=0; ($i<mysql_num_rows($rs)); $i++) {

                //$rowArray = mysql_fetch_row($rs);

                $rowArray = mysql_fetch_object($rs);
                $room = $rowArray->room;
                $h0 = $rowArray->h0;                
                $time = $rowArray->updatedOn;
                
				h0sta($room,$h0,$time);

		}

}



//chkSta();

//echo $argv[1]." ".$argv[2]." ".$argv[3]."\n";

//var_dump($_SERVER['argv']);

$room = $_GET["room"]; //$argv[1];
$sta = $_GET["sta"]; //$argv[2];
$datetime = date("Y-m-d H:i:s");

h0sta($room,$sta,$datetime);



?>



